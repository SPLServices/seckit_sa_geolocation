.. CEF Add on for Splunk documentation master file, created by
   sphinx-quickstart on Sat Oct 13 11:00:10 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to  documentation!
=================================================

This supporting add-on (SA) for Splunk advanced IP information enrichment using the MaxMind family of databases.

https://www.maxmind.com/en/geoip2-databases Supported City2Lite City2 (optional) ISP (optional) ConnectionType (optional)

Installation
-------------------------

* Visit https://www.maxmind.com/en/account/login
   * Create and account
   * Under services generate a license key
   * Note the account ID
   * Generate new key and indicate geoipupdate>v3 record the token provided securely
* Install the SecKit_SA_geolocation on each search head
* Select "Seckit Geolocation from the app menu
* Click the accounts tab and add the account using the account and key provided above
* Optional Add additional databases licensed
* Click the inputs tab
* Edit the input provided
* Select the account created above
* Save
* Enable the input


USAGE
-------------------------

| seckit_iplocation(fieldname)
| seckit_iplocation(fieldname,prefix)
Where fieldname is the name of the field containing the IP prefix is the prefix to assign to the output fields

Example
| seckit_iplocation(src,geo)

FIELDS
-------------------------

city (City/CityLite)
country (City/CityLite)
lat (City/CityLite)
long (City/CityLite)
connection_type (ConnectionType)
isp (ISP)
isp_organization (ISP)
isp_ip (ISP)
isp_asn (ISP)
isp_asn_organization (ISP)

Source
-------------------------

https://bitbucket.org/Splunk-SecPS/seckit_sa_geolocation

Legal
-------------------------

Splunk is a registered trademark of Splunk, Inc.
MaxMind is a registered trademark of MaxMind, Inc.

.. toctree::
   :maxdepth: 2
   :caption: Contents:


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
