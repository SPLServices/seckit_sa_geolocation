
# encoding = utf-8

import os
import sys
import time
import datetime
import json
import tempfile
import subprocess


'''
    IMPORTANT
    Edit only the validate_input and collect_events functions.
    Do not edit any other part in this file.
    This file is generated only once when creating the modular input.
'''
def use_single_instance_mode():
    return True

def validate_input(helper, definition):
    """Implement your own validation logic to validate the input stanza configurations"""
    # This example accesses the modular input variable
    # edition_ids = definition.parameters.get('edition_ids', None)
    # license = definition.parameters.get('license', None)
    pass

def collect_events(helper, ew):
    # get the loglevel from the setup page
    loglevel = helper.get_log_level()
    # get proxy setting configuration
    proxy_settings = helper.get_proxy()

    for stanza_name in helper.get_input_stanza_names():
        if stanza_name=="aob_test" or stanza_name=="main":
            helper.log_info("Stanza is acceptable " + stanza_name)
            opt_edition_ids = helper.get_arg('edition_ids', stanza_name)
            try:
                opt_license = helper.get_arg('license', stanza_name)
            except:
                opt_license = None
                helper.log_info("Can't get account which probably means only free license")


            if opt_license is None:
                account="0"
                license="000000000000"
                helper.log_info("Updating for Account default")
            else:
                account=opt_license["username"]
                license=opt_license["password"]
                helper.log_info("Updating for Account " + account)


#            file = tempfile.NamedTemporaryFile(mode='w',suffix=".conf", prefix="GeoIP")
            with tempfile.NamedTemporaryFile(mode='w',suffix=".conf", prefix="GeoIP") as file:
                file.write("\nAccountID " + account)
                file.write("\nLicenseKey " + license)
                file.write("\nEditionIDs " + opt_edition_ids + "\n")

                if proxy_settings == {}:
                    helper.log_info("no proxy")
                else:
                    file.write("\nProxy " + proxy_settings["proxy_url"] + ":" + proxy_settings["proxy_port"])
                    if not proxy_settings["proxy_username"]  is None:
                        file.write("\nProxyUserPassword " + proxy_settings["proxy_username"] + ":" + proxy_settings["proxy_password"])


                file.flush()
                guargs = os.path.expandvars("-v -d $SPLUNK_HOME/etc/apps/SecKit_SA_geolocation/data/ -f " + file.name)
                helper.log_info(guargs)

                proc = subprocess.Popen(["$SPLUNK_HOME/etc/apps/SecKit_SA_geolocation/bin/geoipupdate/linux_amd64/geoipupdate " + guargs],stderr=subprocess.PIPE,shell=True)

                for line in iter(proc.stderr.readline,''):
                    helper.log_info(line.rstrip())

        else:
            helper.log_critical("Stanza is NOT acceptable " + stanza_name)
