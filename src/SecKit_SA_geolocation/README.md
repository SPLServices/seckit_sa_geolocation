
#Welcome
This supporting add-on (SA) for Splunk advanced IP information enrichment using the MaxMind family of databases.

https://www.maxmind.com/en/geoip2-databases
Supported One of
* 	City2Lite (Free)
* 	City2 (Paid)

Supported Zero or more of
* 	ISP  (optional)
* 	ConnectionType (optional)
*   Anonymous IP

#Installation
* Install the SecKit_SA_geolocation on each search head as needed
* From the SSH Shell execute $SPLUNK_HOME/etc/apps/SecKit_SA_geolocation/bin/SecKit_geo_update.sh

#USAGE

See README.epub

#Legal
* *Splunk* is a registered trademark of Splunk, Inc.
* *MaxMind* is a registered trademark of MaxMind, Inc.

#License

Splunk End User License Agreement see license-eula.txt in root of the package

# Support/Contributing

This application is community supported. See the project repository for
more information include source and issue tracker.
[Repository](https://bitbucket.org/SPLServices/SecKit_SA_geolocation/)


# Binary File Declaration

bin/geoipupdate/linux_amd64/geoipupdate is provided by MaxMind source and releases can be found at [GeoIPUpdate](https://github.com/maxmind/geoipupdate)
